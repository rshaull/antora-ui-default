'use strict'

function isLatest (page) {
  return page.componentVersion.version === page.component.latest.version
}

module.exports = (page) => {
  // refer to src/partials/nav-explore.hbs
  if (!page.componentVersion || !page.component.latest) {
    return ''
  }
  return page.componentVersion.displayVersion + (isLatest(page) ? ' (latest)' : '')
}
